import request from '@/utils/request';

export async function queryCurrent() {
  return request('/api/opay/overlord/currentUser');
}

export async function queryNotices() {
  return request('/api/notices');
}
