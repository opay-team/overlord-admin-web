import request from '@/utils/request';

// 营销系统登录接口
export async function salesLogin(params) {
  return request('/api/opay/overlord/adminLogin', {
    method: 'POST',
    data: params,
  });
}

// 营销系统退出接口
export async function adminLogout() {
  return request('/api/opay/overlord/adminLogout', {
    method: 'GET',
  });
}
