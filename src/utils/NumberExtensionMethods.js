// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable func-names */
// eslint-disable-next-line no-extend-native
Number.prototype.toLocalCurrency = function (
  locale,
  currency,
  showDecimals = true,
) {
  return this.toLocaleString(locale, {
    style: 'currency',
    currency,
    minimumFractionDigits: showDecimals ? 2 : 0,
    maximumFractionDigits: showDecimals ? 2 : 0,
  });
};
