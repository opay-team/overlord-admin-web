import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);

export const dateIsoFormat = date => (date ? dayjs(date).format('YYYY-MM-DDTHH:mm:ss.SSSZ') : '');

const transactionFormat = 'YYYY MMM DD HH:mm:ss [WAT] Z';

export const formatDate = dateString => dayjs(dateString, '').format('MMM DD, hh:mm A'); // Jan 29, 2:24 PM

export const date = (dateString, format) => {
  if (format === true) return dayjs(dateString, transactionFormat);
  return dayjs(dateString, format);
};
