import request from '@/utils/request';

export async function fakeRegister(params) {
  return request('/api/opay/overlord/register', {
    method: 'POST',
    data: params,
  });
}
