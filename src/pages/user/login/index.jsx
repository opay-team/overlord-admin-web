import { Alert } from 'antd';
import React, { Component } from 'react';
import { connect } from 'dva';
// import Link from 'umi/link';
import LoginComponents from './components/Login';
import styles from './style.less';

const { UserName, Password, Submit } = LoginComponents;

@connect(({ login, loading }) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))
class Login extends Component {
  loginForm = undefined;

  state = {
    type: 'account',
  };

  handleSubmit = (err, values) => {
    if (!err) {
      const { dispatch } = this.props;
      dispatch({
        type: 'login/login',
        payload: { ...values },
      });
    }
  };

  renderMessage = content => (
    <Alert
      style={{
        marginBottom: 24,
      }}
      message={content}
      type="error"
      showIcon
    />
  );

  render() {
    const { userLogin, submitting } = this.props;
    const { code, msg } = userLogin;
    console.log(userLogin)
    const { type } = this.state;
    return (
      <div className={styles.main}>
        <LoginComponents
          defaultActiveKey={type}
          onSubmit={this.handleSubmit}
          onCreate={form => {
            this.loginForm = form;
          }}
        >
          {code !== '00' && code !== undefined && !submitting &&
            this.renderMessage(msg)}
          <UserName
            name="email"
            placeholder="userName"
            rules={[
              {
                required: true,
                message: 'Please enter your email!',
              },
            ]}
          />
          <Password
            name="pwd"
            placeholder="password"
            rules={[
              {
                required: true,
                message: 'Please enter your password!',
              },
            ]}
            onPressEnter={e => {
              e.preventDefault();

              if (this.loginForm) {
                this.loginForm.validateFields(this.handleSubmit);
              }
            }}
          />
          <Submit loading={submitting}>
            Login
          </Submit>
          {/* <div className={styles.other}>
            <Link className={styles.register} to="/user/register">
              Sign up
            </Link>
          </div> */}
        </LoginComponents>
      </div>
    );
  }
}

export default Login;
