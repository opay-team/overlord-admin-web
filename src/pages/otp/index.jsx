import { Button, Card, Col, Form, Input, Row, Table } from 'antd';
import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import styles from './style.less';
import { formatDate } from '../../utils/date';

const FormItem = Form.Item;

/* eslint react/no-multi-comp:0 */
@connect(({ otpList, loading, global }) => ({
  otpList,
  loading: loading.models.otpList,
  global,
}))
class Transaction extends Component {
  state = {
    pageData: {
      pageIndex: 1,
      pageSize: 10,
    },
  };

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      width: 140,
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      width: 140,
    },
    {
      title: 'Message Channels',
      dataIndex: 'messageChannels',
      width: 140,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      width: 120,
    },
    {
      title: 'Content',
      dataIndex: 'content',
      width: 120,
    },
    {
      title: 'CreateTime',
      dataIndex: 'createTime',
      width: 200,
      render: text => formatDate(text), // Jan 29, 2:24 PM
    },
    {
      title: 'Language',
      dataIndex: 'language',
      width: 120,
    },
    {
      title: 'Remark',
      dataIndex: 'remark',
      width: 120,
    },
  ];

  componentDidMount() {
    this.getorderList(this.state.pageData);
  }

  getorderList = payload => {
    const { dispatch } = this.props;
    dispatch({
      type: 'otpList/fetch',
      payload,
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      pageData: {
        pageIndex: 1,
        pageSize: 10,
      },
    }, () => {
      this.getorderList(this.state.pageData);
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        ...this.state.pageData,
      };
      this.getorderList(values);
    });
  };

  handleTableChange = pagination => {
    const { form } = this.props;
    const params = {
      pageIndex: pagination.current,
      pageSize: pagination.pageSize,
      ...form,
    };
    this.setState({
      pageData: {
        pageIndex: pagination.current,
        pageSize: pagination.pageSize,
      },
    })
    this.getorderList(params);
  };

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row>
          <Col md={8} sm={24}>
            <FormItem label="Phone Number">
              {getFieldDecorator('phoneNumber')(<Input placeholder="Phone Number" />)}
            </FormItem>
          </Col>
        </Row>
        <div
          style={{
            overflow: 'hidden',
          }}
        >
          <div
            style={{
              float: 'right',
              marginBottom: 24,
            }}
          >
            <Button type="primary" htmlType="submit">
              search
            </Button>
            <Button
              style={{
                marginLeft: 8,
              }}
              onClick={this.handleFormReset}
            >
              reset
            </Button>
          </div>
        </div>
      </Form>
    );
  }

  render() {
    const {
      otpList: { data },
      loading,
    } = this.props;

    const { lists = [], totalCount = 0 } = data || {};

    const paginationProps = totalCount
      ? {
          showSizeChanger: true,
          showQuickJumper: true,
          total: totalCount,
        }
      : false;

    return (
      <PageHeaderWrapper>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderAdvancedForm()}</div>
            <Table
              loading={loading}
              dataSource={lists}
              columns={this.columns}
              pagination={paginationProps}
              onChange={this.handleTableChange}
              rowKey="activityId"
              scroll={{ x: 1000 }}
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Form.create()(Transaction);
