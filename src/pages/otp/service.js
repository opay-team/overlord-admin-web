import request from '@/utils/request';

export async function queryOtpList(params) {
  return request('/api/opay/overlord/otpList', {
    method: 'POST',
    data: params,
  });
}
