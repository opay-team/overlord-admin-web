import { queryUserList } from './service';

const Model = {
  namespace: 'UserList',
  state: {
    data: {
      lists: [],
      totalCount: 0,
    },
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryUserList, payload);
      if (response.code === '00') {
        yield put({
          type: 'save',
          payload: response.data,
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return { ...state, data: action.payload };
    },
  },
};
export default Model;
