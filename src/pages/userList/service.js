import request from '@/utils/request';

// 营销活动列表查询接口
export async function queryUserList(params) {
  return request('/api/opay/overlord/appUserList', {
    method: 'POST',
    data: params,
  });
}
