import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Row,
} from 'antd';
import React, { Component } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect } from 'dva';
import { formatDate } from '../../utils/date';
import StandardTable from './components/StandardTable';
import styles from './style.less';

const FormItem = Form.Item;

/* eslint react/no-multi-comp:0 */
@connect(({ UserList, loading }) => ({
  UserList,
  loading: loading.models.UserList,
}))
class Activity extends Component {
  state = {
    formValues: {},
    pageData: {
      pageIndex: 1,
      pageSize: 10,
    },
  };

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      align: 'center',
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      align: 'center',
    },
    {
      title: 'Country',
      dataIndex: 'country',
      align: 'center',
    },
    {
      title: 'Activity Begin Time',
      dataIndex: 'beginTime',
      align: 'center',
      render: text => formatDate(text), // Jan 29, 2:24 PM
    },
  ];

  componentDidMount() {
    this.activityListFetch(this.state.pageData);
  }

  activityListFetch = payload => {
    const { dispatch } = this.props;
    dispatch({
      type: 'UserList/fetch',
      payload,
    });
  }

  handleStandardTableChange = pagination => {
    const { formValues } = this.state;
    this.setState({
      pageData: {
        pageIndex: pagination.current,
        pageSize: pagination.pageSize,
      },
    });
    const params = {
      pageIndex: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
    };

    this.activityListFetch(params);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    this.activityListFetch(this.state.pageData);
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        pageIndex: 1,
        pageSize: 10,
      };
      this.activityListFetch(values);
      // delete values.pageIndex;
      // delete values.pageSize;
      this.setState({
        formValues: values,
      });
    });
  };

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row
          gutter={{
            md: 8,
            lg: 24,
            xl: 48,
          }}
        >
          <Col md={8} sm={24}>
            <FormItem label="Phone Number">
              {getFieldDecorator('phoneNumber')(<Input placeholder="Phone Number" />)}
            </FormItem>
          </Col>
        </Row>
        <div
          style={{
            overflow: 'hidden',
          }}
        >
          <div
            style={{
              float: 'right',
              marginBottom: 24,
            }}
          >
            <Button type="primary" htmlType="submit">
              search
            </Button>
            <Button
              style={{
                marginLeft: 8,
              }}
              onClick={this.handleFormReset}
            >
              reset
            </Button>
          </div>
        </div>
      </Form>
    );
  }

  render() {
    const {
      UserList: { data },
      loading,
    } = this.props;

    return (
      <PageHeaderWrapper title="Activity">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderAdvancedForm()}</div>
            <StandardTable
              loading={loading}
              data={data}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
              rowKey="number"
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default Form.create()(Activity);
